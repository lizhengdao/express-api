const express = require('express')
const app = express()
const bcrypt = require('bcrypt')

// Tell express app that is able to use json
app.use(express.json())

const users = [];

// Obtain all users 
app.get('/users', (req, res) => {
    res.json(users)
});

// Create users
app.post('/users', async (req, res) => {
    try {
        const hashedPassword = await bcrypt.hash(req.body.pass, 10)
        const user = { user: req.body.user, pass: hashedPassword}
        users.push(user)
        res.status(201).send()

    } catch (error) {
        res.status(500).send()
    }
});

// Login user
app.post('/users/login', async (req, res) => {
    const user = users.find(user => user.user === req.body.user)
    if (user == null) {
        return res.status(400).send('Cannot find user')
    }
    try {
        if (await bcrypt.compare(req.body.pass, user.pass)) {
            res.send('Success')
        }else{
            res.send('Not allowed')
        }
    } catch (error) {
        res.status(500).send()
    }
})
app.listen(3000)